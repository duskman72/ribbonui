#include "ribbongallery.h"
#include <QPainter>
#include <QCursor>
#include <QScrollArea>
#include <QDebug>

#include "ribbonbutton.h"

RibbonGallery::RibbonGallery(QWidget *parent) :
    QWidget(parent),RibbonControl(this),mSizeType(RibbonUI::RIBBONTYPE_LARGE),mDefaultAction(0)
{
    setFixedHeight(70);
    setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    mStack = new QStackedLayout();
    mStack->setSizeConstraint(QLayout::SetNoConstraint);
    setLayout(mStack);

    QScrollArea *area = new QScrollArea(this);
    mStack->addWidget(area);
    area->setStyleSheet("QScrollArea {background: #ffffff;border: 1px solid "+RibbonUI::GALLERY_BORDER_COLOR+"}");
    area->setFixedHeight(70);
    area->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    area->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);

    RibbonGalleryScrollBar *sb = new RibbonGalleryScrollBar();
    area->setVerticalScrollBar(sb);

    RibbonGalleryCornerWidget *corner = new RibbonGalleryCornerWidget();
    area->setCornerWidget(corner);
    connect(corner, SIGNAL(clicked()), this, SLOT(onCornerWidgetClicked()));

    QWidget *wid = new QWidget();
    area->setWidgetResizable(true);
    area->setWidget(wid);
    mGalleryLayout = new RibbonGalleryLayout();
    wid->setObjectName("galleryholder");
    wid->setStyleSheet("QWidget#galleryholder {background: #FFFFFF;}");
    wid->setLayout(mGalleryLayout);
    wid->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    wid->setContentsMargins(0, 0, 0, 0);

    RibbonButton *btn = new RibbonButton();
    btn->setSizeType(RibbonUI::RIBBONTYPE_LARGE);
    btn->setArrowType(Qt::DownArrow);
    connect(btn, SIGNAL(clicked()), this, SLOT(onCornerWidgetClicked()));
    mStack->addWidget(btn);

    mStack->setCurrentIndex(0);
}

void RibbonGallery::setSizeType(RibbonUI::RibbonSizeType type) {
    switch (type) {
    case RibbonUI::RIBBONTYPE_LARGE:
        mSizeType = type;
        mStack->setCurrentIndex(0);
        break;

    case RibbonUI::RIBBONTYPE_MEDIUM:
    case RibbonUI::RIBBONTYPE_SMALL:
        mSizeType = RibbonUI::RIBBONTYPE_MEDIUM;
        mStack->setCurrentIndex(1);
        break;
    }
    updateGeometry();
}

QSize RibbonGallery::sizeHint() const {
    if (mStack->currentIndex() == 0) {
        QSize size = mStack->currentWidget()->sizeHint();
        size.setWidth(size.width()+7);
        return size;
    } else {
        return mStack->currentWidget()->sizeHint();
    }
}

RibbonUI::RibbonSizeType RibbonGallery::sizeType() const {
    return mSizeType;
}

void RibbonGallery::addItem(RibbonGalleryItem *item) {
    mGalleryLayout->addWidget(item);
}

void RibbonGallery::addAction(QAction *action) {
    RibbonGalleryItem *item = new RibbonGalleryItem(this);
    item->setIcon(action->icon());
    item->setText(action->text());
    mGalleryLayout->addWidget(item);
}

void RibbonGallery::setIcon(const QIcon &icon) {
    RibbonButton *btn = reinterpret_cast<RibbonButton *>(mStack->widget(1));
    btn->setIcon(icon);
}

void RibbonGallery::setText(const QString &text) {
    RibbonButton *btn = reinterpret_cast<RibbonButton *>(mStack->widget(1));
    btn->setText(text);
}

void RibbonGallery::setWidget(QWidget *wid) {
    QScrollArea *area = reinterpret_cast<QScrollArea *>(mStack->widget(0));
    area->setWidget(wid);
}

void RibbonGallery::onCornerWidgetClicked() {
    QScrollArea *area = reinterpret_cast<QScrollArea *>(mStack->widget(0));

    QWidget *wid = area->takeWidget();
    RibbonGalleryPopup* popupWidget = new RibbonGalleryPopup();
    connect(popupWidget, SIGNAL(closed()), this, SLOT(onPopupClose()));
    popupWidget->layout()->addWidget(wid);
    if ((sizeType() & RibbonUI::RIBBONTYPE_LARGE) == RibbonUI::RIBBONTYPE_LARGE) {
        popupWidget->move(parentWidget()->mapToGlobal(pos()));
    } else {
        RibbonButton *btn = reinterpret_cast<RibbonButton *>(mStack->widget(1));
        QPoint btnPos = btn->pos();
        btnPos.setY(btnPos.y()+btn->sizeHint().height());
        popupWidget->move(btn->mapToGlobal(btnPos));
    }
    popupWidget->show();
}

void RibbonGallery::onPopupClose() {
    QScrollArea *area = reinterpret_cast<QScrollArea *>(mStack->widget(0));
    RibbonGalleryPopup *popup = reinterpret_cast<RibbonGalleryPopup *>(sender());
    QWidget *content = popup->layout()->takeAt(0)->widget();
    area->setWidget(content);
    if ((sizeType() & RibbonUI::RIBBONTYPE_LARGE) != RibbonUI::RIBBONTYPE_LARGE) {
        RibbonButton *btn = reinterpret_cast<RibbonButton *>(mStack->widget(1));
        btn->update();
    }
}
