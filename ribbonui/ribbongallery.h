#ifndef RIBBONGALLERY_H
#define RIBBONGALLERY_H

#include <QScrollArea>
#include <QScrollBar>
#include <QMouseEvent>
#include <QCloseEvent>
#include <QPushButton>
#include <QStackedLayout>
#include <QAction>
#include "ribbondefs.h"
#include "ribbongallerylayout.h"
#include "ribbongallerypopup.h"
#include "ribbongalleryscrollbar.h"
#include "ribbongallerycornerwidget.h"
#include "ribbongalleryitem.h"
#include "ribboncontrol.h"

class RIBBONUI_EXPORT RibbonGallery : public QWidget,
                                      public RibbonControl
{
    Q_OBJECT
public:
    explicit RibbonGallery(QWidget *parent = 0);
    void setSizeType(RibbonUI::RibbonSizeType type);
    RibbonUI::RibbonSizeType sizeType() const;
    void addItem(RibbonGalleryItem *item);
    void addAction(QAction *action);
    void setWidget(QWidget *wid);
    void setIcon(const QIcon &icon);
    void setText(const QString &text);
    QSize sizeHint() const;

signals:

public slots:
    void onCornerWidgetClicked();
    void onPopupClose();

private:
    RibbonUI::RibbonSizeType mSizeType;
    QStackedLayout *mStack;
    RibbonGalleryLayout *mGalleryLayout;
    QAction *mDefaultAction;
};

#endif // RIBBONGALLERY_H
