#include "ribbontablayout.h"
#include <QWidget>

RibbonTabLayout::RibbonTabLayout(QWidget *parent) :
    QLayout(parent) {
}

RibbonTabLayout::~RibbonTabLayout() {
    QLayoutItem *item;
    while ((item = takeAt(0)) != 0) {
        removeWidget(item->widget());
        delete item->widget();
    }
}

void RibbonTabLayout::addItem(QLayoutItem *item) {
    items.append(item);
}

QSize RibbonTabLayout::sizeHint() const {
    QLayoutItem *item;
    int width = 5;
    for (int i = 0; i < items.size(); i++) {
        item = itemAt(i);
        width += item->sizeHint().width();
        if (i == 0)
            width += 10;
    }
    return QSize(width, 25);
}

QLayoutItem *RibbonTabLayout::itemAt(int index) const {
    return items.length() > index ? items.at(index) : 0;
}

QLayoutItem *RibbonTabLayout::takeAt(int index) {
    return items.length() > index ? items.takeAt(index) : 0;
}

int RibbonTabLayout::count() const {
    return items.size();
}

void RibbonTabLayout::setGeometry(const QRect &rect) {
    QLayout::setGeometry(rect);
    doLayout(rect);
}

void RibbonTabLayout::doLayout(const QRect &rect) {
    QLayoutItem *item;
    int l, t, r, b;
    getContentsMargins(&l, &t, &r, &b);
    int x = 0;
    int y = rect.y() + 5;

    for (int i = 0; i < items.size(); i++) {
        item = itemAt(i);
        item->setGeometry(QRect(QPoint(x, y), item->sizeHint()));
        x += item->sizeHint().width();
        if (i == 0)
            x += 5;
    }
}
