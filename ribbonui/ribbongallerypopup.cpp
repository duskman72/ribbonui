#include "ribbongallerypopup.h"
#include <QPainter>
#include <QCursor>
#include <QHBoxLayout>
#include <QGuiApplication>
#include <QApplication>
#include <QDesktopWidget>
#include <QScreen>

RibbonGalleryPopup::RibbonGalleryPopup(QWidget *parent) :
    QWidget(parent, Qt::WindowFlags(Qt::Popup)) {
    setAttribute(Qt::WA_DeleteOnClose);

    setContentsMargins(0, 0, 0, 0);
    QHBoxLayout *l = new QHBoxLayout();
    l->setContentsMargins(1, 1, 1, 1);
    setLayout(l);
}

void RibbonGalleryPopup::closeEvent(QCloseEvent *e) {
    QWidget::closeEvent(e);
    emit closed();
}

void RibbonGalleryPopup::paintEvent(QPaintEvent *) {
    QPainter p(this);
    p.setRenderHint(QPainter::HighQualityAntialiasing);
    p.fillRect(rect(), Qt::white);

    QPen pen;
    pen.setWidth(1);
    pen.setBrush(QColor(RibbonUI::BORDER_COLOR));
    p.setPen(pen);

    QRect r = rect();
    r.setRight(r.right()-1);
    r.setBottom(r.bottom()-1);
    p.setBrush(Qt::NoBrush);
    p.drawRect(r);
}
