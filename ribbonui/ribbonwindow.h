#ifndef RIBBONWINDOW_H
#define RIBBONWINDOW_H

#include <QMainWindow>
#include <QPainter>
#include <QEvent>
#include <QMouseEvent>
#include <QShowEvent>
#include <QLabel>
#include "ribbonbar.h"
#include "ribbonstatusbar.h"
#include "ribbondefs.h"

#ifdef Q_OS_WIN32
#include <windows.h>
#include <winuser.h>
#include <dwmapi.h>
#endif

class RIBBONUI_EXPORT RibbonWindowHandle: public QLabel {
    Q_OBJECT
public:
    explicit RibbonWindowHandle(QWidget *parent = 0);

signals:
    void moveRequest(QMouseEvent *);
    void moveEvent(QMouseEvent *);
    void doubleClickEvent(QMouseEvent *);

protected:
    void mousePressEvent(QMouseEvent *e);
    void mouseDoubleClickEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);

private:
    bool down;
};

class RIBBONUI_EXPORT RibbonWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit RibbonWindow(QWidget *parent = 0);
    virtual ~RibbonWindow();
    void setMoveHandle(RibbonWindowHandle *handle);
    RibbonBar *ribbonBar();
    void setWindowIcon(const QIcon &icon);

signals:

public slots:
    void setWindowTitle(const QString &title);
    void show();
    void showNormal();
    void showMinimized();
    void showMaximized();
    void minimize();
    void maximize();
    void onWindowMoveRequest(QMouseEvent *e);
    void onWindowMove(QMouseEvent *e);
    void onResizeWindow(QMouseEvent *e);

protected:
    void changeEvent(QEvent *e);
#ifdef Q_OS_WIN32
    bool event(QEvent *event);
#endif

private:
    QPoint oldPos;
    RibbonBar *mRibbonBar;
    RibbonWindowHandle *mMoveHandle;
};

#endif // RIBBONWINDOW_H
