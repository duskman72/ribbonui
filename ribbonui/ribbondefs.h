#ifndef RIBBONDEFS_H
#define RIBBONDEFS_H

#include <QtCore/QtGlobal>
#include <QString>

#if defined(RIBBONUI_LIB)
#  define RIBBONUI_EXPORT Q_DECL_EXPORT
#else
#  define RIBBONUI_EXPORT Q_DECL_IMPORT
#endif

namespace RibbonUI {
    static const QString BORDER_COLOR = "#D1D1D1";
    static const QString GALLERY_BORDER_COLOR = "#AAAAAA";
    static const QString GALLERY_BORDER_COLOR_DISABLED = "#DDDDDD";
    static const QString MENU_STYLESHEET = "QMenu {background-color: #FFFFFF;border: 1px solid #CCCCCC;margin: 0;padding: 0;} QMenu::icon {padding: 2px;margin: 2px;} QMenu::item {background-color: #ffffff;color: #333333;padding: 5px 65px 5px 26px;border: 1px solid transparent;} QMenu::item:selected {background-color: #cde6f7;} QMenu::icon:checked {background-color: #cde6f7;border: 1px solid #92c0e0;}";
    enum RibbonSizeType {
        RIBBONTYPE_SMALL = 1,
        RIBBONTYPE_MEDIUM = 2,
        RIBBONTYPE_LARGE = 4
    };
}

#endif // RIBBONDEFS_H
