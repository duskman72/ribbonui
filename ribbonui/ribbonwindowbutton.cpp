#include "ribbonwindowbutton.h"

RibbonWindowButton::RibbonWindowButton(QWidget *parent) :
    QPushButton(parent),hovered(false),mButtonState(CLOSE)
{
    setFixedSize(30, 22);
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

void RibbonWindowButton::setButtonState(RibbonWindowButton::ButtonState state) {
    mButtonState = state;
    update();
}

QSize RibbonWindowButton::minimumSize() const {
    return QSize(30, 22);
}

QSize RibbonWindowButton::maximumSize() const {
    return QSize(30, 22);
}

void RibbonWindowButton::enterEvent(QEvent *) {
    hovered = true;
    update();
}

void RibbonWindowButton::leaveEvent(QEvent *) {
    hovered = false;
    update();
}

void RibbonWindowButton::paintEvent(QPaintEvent *) {
    QPainter p(this);
    QPen pen;
    pen.setWidth(2);
    pen.setBrush(Qt::gray);

    // draw hover / down state
    p.fillRect(rect(), Qt::white);
    pen.setBrush(Qt::gray);

    if (hovered) {
        p.fillRect(rect(), QColor("#cde6f7"));
        pen.setBrush(QColor("#2a8dd4"));
    }

    if (isDown()) {
        p.fillRect(rect(), QColor("#b1d6f0"));
        pen.setBrush(QColor("#2a8dd4").darker(100));
    }

    // draw button type
    p.setRenderHint(QPainter::HighQualityAntialiasing, true);
    switch (mButtonState) {
    case CLOSE:
        p.setPen(pen);

        p.drawLine(9, 6, 18, 16);
        p.drawLine(18, 6, 9, 16);
        break;
    case MINIMIZE:
        pen.setWidth(2);
        p.setPen(pen);
        p.drawLine(9, 14, 18, 14);
        break;
    case MAXIMIZE:
        pen.setWidth(3);
        p.setPen(pen);
        p.drawLine(9, 6, 19, 6);

        pen.setWidth(1);
        p.setPen(pen);
        // bottom line
        p.drawLine(9, 16, 19, 16);
        // left line
        p.drawLine(8, 6, 8, 16);
        // right line
        p.drawLine(20, 6, 20, 16);
        break;
    case MAXIMIZED:
        pen.setWidth(2);
        p.setPen(pen);
        // back window
        p.drawLine(11, 6, 19, 6);

        // front window
        p.drawLine(9, 9, 17, 9);

        // left back window line
        pen.setWidth(1);
        p.setPen(pen);
        p.drawLine(10, 6, 10, 9);

        // right back window line
        p.drawLine(19, 6, 19, 13);

        // bottom back window line
        p.drawLine(17, 13, 18, 13);

        // left front window line
        p.drawLine(8, 9, 8, 16);

        // right front window line
        p.drawLine(17, 9, 17, 16);

        // bottom front window line
        p.drawLine(8, 16, 16, 16);
        break;
    }
}
