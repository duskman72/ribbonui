#include "ribbonstatusbar.h"

RibbonStatusBar::RibbonStatusBar(QWidget *parent) :
    QStatusBar(parent)
{
    QPalette pal = palette();
    setAutoFillBackground(true);
    pal.setBrush(QPalette::Background, QColor("#0072c6"));
    setPalette(pal);

    setStyleSheet("QLabel {color: #ffffff; padding: 3px;text-transform: uppercase;} QStatusBar:item {border: none;}");
}
