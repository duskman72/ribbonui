#include "ribbonquickaccessbar.h"
#include <QPainter>
#include <QMenu>
#include <QDebug>

static const int ACTION_RESTORE = 1;
static const int ACTION_MOVE = 2;
static const int ACTION_RESIZE = 3;
static const int ACTION_MINIMIZE = 4;
static const int ACTION_MAXIMIZE = 5;
static const int ACTION_CLOSE = 6;

RibbonQuickbar::RibbonQuickbar(QWidget *parent) :
    QLayout(parent)
{
    setSpacing(8);
    QPen pen;
    QPixmap pm1(32, 32);
    QPainter p(&pm1);
    p.fillRect(pm1.rect(), QColor("#0072c6"));
    pen.setBrush(Qt::white);
    p.setBrush(Qt::white);
    p.setPen(pen);
    QFont f = p.font();
    f.setPixelSize(24);
    p.setFont(f);
    p.drawText(10, 25, "?");

    windowIcon = new RibbonQuickAccessButton();
    windowIcon->setIcon(QIcon(pm1));
    windowIcon->setArrowVisible(false);
    addWidget(windowIcon);

    QPixmap pm2(18, 18);
    pm2.fill(Qt::transparent);
    QPainter p2(&pm2);
    pen.setBrush(Qt::darkGray);
    pen.setWidth(1);
    p2.setPen(pen);
    p2.drawLine(5, 5, 12, 5);
    QPointF triangle[] = {
        QPointF(4, 8),
        QPointF(13, 8),
        QPointF(9, 12)
    };
    p2.setPen(Qt::NoPen);
    p2.setBrush(Qt::darkGray);
    p2.drawPolygon(triangle, 3);

    quickaccessMenuIcon = new RibbonQuickAccessButton();
    quickaccessMenuIcon->setIcon(QIcon(pm2));
    addWidget(quickaccessMenuIcon);

    QMenu *menu = new QMenu();
    QAction *action1 = new QAction("Wiederherstellen",0);
    QAction *action2 = new QAction("Verschieben",0);
    QAction *action3 = new QAction("Größe ändern",0);
    QAction *action4 = new QAction("Minimieren",0);
    QAction *action5 = new QAction("Maximieren",0);
    QAction *action6 = new QAction("Schließen",0);

    action2->setDisabled(true);
    action3->setDisabled(true);

    action1->setData(ACTION_RESTORE);
    action2->setData(ACTION_MOVE);
    action3->setData(ACTION_RESIZE);
    action4->setData(ACTION_MINIMIZE);
    action5->setData(ACTION_MAXIMIZE);
    action6->setData(ACTION_CLOSE);

    menu->addAction(action1);
    menu->addAction(action2);
    menu->addAction(action3);
    menu->addAction(action4);
    menu->addAction(action5);
    menu->addSeparator();
    menu->addAction(action6);

    windowIcon->setMenu(menu);
}

QSize RibbonQuickbar::sizeHint() const {
    int w = 6;
    for (int i = 0; i < count(); i++) {
        w += itemAt(i)->sizeHint().width() + spacing();
    }

    return QSize(w, 20);
}

QSize RibbonQuickbar::minimumSize() const {
    return sizeHint();
}

QSize RibbonQuickbar::maximumSize() const {
    return sizeHint();
}

void RibbonQuickbar::addItem(QLayoutItem *item) {
    if (layoutitems.length() < 2)
        layoutitems.append(item);
    else
        layoutitems.insert(layoutitems.length()-1, item);
}

void RibbonQuickbar::addWidget(RibbonQuickAccessButton *w) {
    QLayout::addWidget(w);
}

QLayoutItem *RibbonQuickbar::itemAt(int index) const {
    if (count() == 0)
        return 0;

    return layoutitems.length() > index ? layoutitems.at(index) : 0;
}

QLayoutItem *RibbonQuickbar::takeAt(int index) {
    if (count() == 0)
        return 0;

    return layoutitems.length() > index ? layoutitems.takeAt(index) : 0;
}

int RibbonQuickbar::count() const {
    return layoutitems.length();
}

void RibbonQuickbar::setWindowIcon(const QIcon &icon) {
    windowIcon->setIcon(icon);
}

void RibbonQuickbar::setGeometry(const QRect &rect) {
    QLayout::setGeometry(rect);
    doLayout(rect);
}

void RibbonQuickbar::doLayout(const QRect &rect) {
    Q_UNUSED(rect);
    QLayoutItem *item;
    int x = 6;
    int y = 6;
    foreach (item, layoutitems) {
        item->setGeometry(QRect(QPoint(x, y), item->sizeHint()));
        x += item->sizeHint().width() + spacing();
    }
}
