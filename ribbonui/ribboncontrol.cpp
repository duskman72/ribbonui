#include "ribboncontrol.h"

RibbonControl::RibbonControl(QWidget *parent):
    mParent(parent),mTooltip(0) {
}

void RibbonControl::setToolTipWidget(QWidget *wid) {
    mTooltip = wid;
}

QWidget *RibbonControl::toolTipWidget() {
    return mTooltip;
}

QWidget *RibbonControl::controlParent() {
    return mParent;
}
