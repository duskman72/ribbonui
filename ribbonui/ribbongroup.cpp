#include "ribbongroup.h"
#include "ribbonbutton.h"
#include "ribbongallery.h"
#include <QLabel>
#include <QString>
#include <QDebug>

static const QString RIBBON_TITLE_OBJECT = "ribbonUiGroupTitle";

RibbonGroup::RibbonGroup(const QString &title, QWidget *parent) :
    QLayout(parent),mTitle(title),canlayout(true)
{
    setContentsMargins(0, 0, 0, 0);
    QLabel *titleLabel = new QLabel();
    addWidget(titleLabel);
    titleLabel->setText(mTitle);
    titleLabel->setObjectName(RIBBON_TITLE_OBJECT);
    titleLabel->setStyleSheet("QLabel#"+RIBBON_TITLE_OBJECT+" {color: #666666;}");
    titleLabel->setContentsMargins(0, 0, 0, 0);
    titleLabel->setAlignment(Qt::AlignCenter);
    titleLabel->ensurePolished();
}

RibbonGroup::~RibbonGroup() {
    canlayout = false;
}

void RibbonGroup::addItem(QLayoutItem *item) {
    items.append(item);
}

QSize RibbonGroup::sizeHint() const {
    int width = 3;
    static const int height = 90;

    int y = 0;
    int top = 0;
    if (items.length() > 1) {
        int row = 0;
        for (int i = 1; i < items.length(); i++) {
            QLayoutItem *item = itemAt(i);
            RibbonControl *control = dynamic_cast<RibbonControl*>(item->widget());
            if (control != 0) {
                if ((control->sizeType() & RibbonUI::RIBBONTYPE_SMALL) == RibbonUI::RIBBONTYPE_SMALL) {
                    if (row == 0) {
                        y += item->sizeHint().height();
                        row++;
                        if (itemAt(i+1) != 0) {
                            RibbonControl *scontrol = dynamic_cast<RibbonControl*>(itemAt(i+1)->widget());
                            if (scontrol == 0 || ((scontrol->sizeType() & RibbonUI::RIBBONTYPE_SMALL) != RibbonUI::RIBBONTYPE_SMALL)) {
                                // reset to next item
                                row = 0;
                                y = top;
                                width += item->sizeHint().width();
                                if (items.length() > i+1)
                                    width += 2;
                            }
                        } else {
                            width += item->sizeHint().width();
                        }
                    }
                    else
                    if (row == 1) {
                        y += item->sizeHint().height();
                        row++;
                        if (itemAt(i+1) != 0) {
                            RibbonControl *scontrol = dynamic_cast<RibbonControl*>(itemAt(i+1)->widget());
                            if (scontrol == 0 || ((scontrol->sizeType() & RibbonUI::RIBBONTYPE_SMALL) != RibbonUI::RIBBONTYPE_SMALL)) {
                                // reset to next item
                                row = 0;
                                y = top;
                                QLayoutItem *prev0 = itemAt(i-1);
                                if (prev0 != 0 && prev0->sizeHint().width() > item->sizeHint().width()) {
                                    width += prev0->sizeHint().width();
                                } else {
                                    width += item->sizeHint().width();
                                }
                                if (items.length() > i+1)
                                    width += 2;
                            }
                        } else {
                            QLayoutItem *prev0 = itemAt(i-1);
                            if (prev0 != 0 && prev0->sizeHint().width() > item->sizeHint().width()) {
                                width += prev0->sizeHint().width();
                            } else {
                                width += item->sizeHint().width();
                            }
                        }
                    } else {
                        QLayoutItem *prev0 = itemAt(i-2);
                        QLayoutItem *prev1 = itemAt(i-1);
                        int nextWidth0, nextWidth1 = 0;
                        if (prev0 != 0 && prev0->sizeHint().width() > item->sizeHint().width()) {
                            nextWidth0 = prev0->sizeHint().width();
                        } else {
                            nextWidth0 = item->sizeHint().width();
                        }

                        if (prev1 != 0 && prev1->sizeHint().width() > item->sizeHint().width()) {
                            nextWidth1 = prev1->sizeHint().width();
                        } else {
                            nextWidth1 = item->sizeHint().width();
                        }

                        // reset to next item
                        width += qMax(nextWidth0, nextWidth1);
                        // reset to next item
                        row = 0;
                        y = top;
                        if (items.length() > i+1)
                            width += 2;
                    }
                } else { // assume normal sized widget
                    // reset to next item
                    row = 0;
                    y = top;
                    width += item->sizeHint().width();
                    if (items.length() > i+1)
                        width += 2;
                }
            } else { // assume normal sized widget (cant be casted to RibbonControl)
                // reset to next item
                row = 0;
                y = top;
                width += item->sizeHint().width();
                if (items.length() > i+1)
                    width += 2;
            }
        }
    } else {
        QLayoutItem *item = itemAt(0);
        QSize size = item->sizeHint();
        size.setWidth(size.width()+6);
        size.setHeight(height);
        return size;
    }

    return QSize(width, height);
}

QLayoutItem *RibbonGroup::itemAt(int index) const {
    return items.length() > index ? items.at(index) : 0;
}

QLayoutItem *RibbonGroup::takeAt(int index) {
    return items.length() > index ? items.takeAt(index) : 0;
}

int RibbonGroup::count() const {
    return items.size();
}

void RibbonGroup::setGeometry(const QRect &rect) {
    QLayout::setGeometry(rect);
    if (canlayout)
        doLayout(rect);
}

void RibbonGroup::doLayout(const QRect &rect) {
    if (!canlayout)
        return;

    int x = rect.x() + 3;
    int y = rect.top();
    int top = rect.top();
    if (items.length() > 1) {
        int row = 0;
        for (int i = 1; i < items.length(); i++) {
            QLayoutItem *item = itemAt(i);
            RibbonControl *control = dynamic_cast<RibbonControl*>(item->widget());
            if (control != 0) {
                if ((control->sizeType() & RibbonUI::RIBBONTYPE_SMALL) == RibbonUI::RIBBONTYPE_SMALL) {
                    if (row == 0) {
                        item->setGeometry(QRect(QPoint(x, y), item->sizeHint()));
                        y += item->sizeHint().height();
                        row++;
                        if (itemAt(i+1) != 0) {
                            RibbonControl *scontrol = dynamic_cast<RibbonControl*>(itemAt(i+1)->widget());
                            if (scontrol == 0 || ((scontrol->sizeType() & RibbonUI::RIBBONTYPE_SMALL) != RibbonUI::RIBBONTYPE_SMALL)) {
                                // reset to next item
                                row = 0;
                                y = top;
                                x += item->sizeHint().width();
                                x += 2;
                            }
                        }
                    }
                    else
                    if (row == 1) {
                        item->setGeometry(QRect(QPoint(x, y), item->sizeHint()));
                        y += item->sizeHint().height();
                        row++;
                        if (itemAt(i+1) != 0) {
                            RibbonControl *scontrol = dynamic_cast<RibbonControl*>(itemAt(i+1)->widget());
                            if (scontrol == 0 || ((scontrol->sizeType() & RibbonUI::RIBBONTYPE_SMALL) != RibbonUI::RIBBONTYPE_SMALL)) {
                                // reset to next item
                                row = 0;
                                y = top;
                                QLayoutItem *prev0 = itemAt(i-1);
                                if (prev0 != 0 && prev0->sizeHint().width() > item->sizeHint().width()) {
                                    x += prev0->sizeHint().width();
                                } else {
                                    x += item->sizeHint().width();
                                }

                                // reset to next item
                                row = 0;
                                y = top;
                                x += 2;
                            }
                        }
                    } else {
                        item->setGeometry(QRect(QPoint(x, y), item->sizeHint()));
                        QLayoutItem *prev0 = itemAt(i-2);
                        QLayoutItem *prev1 = itemAt(i-1);
                        int nextWidth0, nextWidth1 = 0;
                        if (prev0 != 0 && prev0->sizeHint().width() > item->sizeHint().width()) {
                            nextWidth0 = prev0->sizeHint().width();
                        } else {
                            nextWidth0 = item->sizeHint().width();
                        }

                        if (prev1 != 0 && prev1->sizeHint().width() > item->sizeHint().width()) {
                            nextWidth1 = prev1->sizeHint().width();
                        } else {
                            nextWidth1 = item->sizeHint().width();
                        }

                        // reset to next item
                        x += qMax(nextWidth0, nextWidth1);
                        row = 0;
                        y = top;
                        x += 2;
                    }
                } else { // assume normal sized widget
                    // reset to next item
                    row = 0;
                    y = top;
                    item->setGeometry(QRect(QPoint(x, y), item->sizeHint()));
                    x += item->sizeHint().width();
                    x += 2;
                }
            } else { // assume normal sized widget (cant be casted to RibbonControl)
                // reset to next item
                row = 0;
                y = top;
                item->setGeometry(QRect(QPoint(x, y), item->sizeHint()));
                x += item->sizeHint().width();
                x += 2;
            }
        }
    }

    // layout ribbon title
    QLayoutItem *item = itemAt(0);
    x = rect.x() + 3;
    y = sizeHint().height() - item->sizeHint().height();
    if (x <= 0)
        x = 5;

    if (item->widget() != 0 && parentWidget() != 0) {
        item->widget()->setParent(parentWidget());
        item->setGeometry(QRect(x, y, sizeHint().width()-3, item->sizeHint().height()));
    }
}
