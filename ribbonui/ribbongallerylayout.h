#ifndef RIBBONGALLERYLAYOUT_H
#define RIBBONGALLERYLAYOUT_H

#include <QLayout>
#include "ribbongalleryitem.h"
#include "ribbondefs.h"

class RIBBONUI_EXPORT RibbonGalleryLayout : public QLayout {
    Q_OBJECT
public:
    explicit RibbonGalleryLayout(QWidget *parent = 0);
    virtual ~RibbonGalleryLayout();
    void addItem(QLayoutItem *item);
    void addWidget(RibbonGalleryItem *item);
    void setGeometry(const QRect &rect);

    QSize sizeHint() const;
    QSize minimumSize() const;
    QLayoutItem *itemAt(int index) const;
    QLayoutItem *takeAt(int index);
    int count() const;

signals:

public slots:

private:
    QList<QLayoutItem *> items;
    void doLayout();
};

#endif // RIBBONGALLERYLAYOUT_H
