#ifndef RIBBONBUTTON_H
#define RIBBONBUTTON_H

#include <QToolButton>
#include <QMouseEvent>
#include <QMenu>
#include "ribboncontrol.h"
#include "ribbondefs.h"

class RIBBONUI_EXPORT RibbonButton : public QToolButton,
                                     public RibbonControl
{
    Q_OBJECT
public:
    explicit RibbonButton(QWidget *parent = 0);
    virtual ~RibbonButton(){}
    void setSizeType(RibbonUI::RibbonSizeType type);
    RibbonUI::RibbonSizeType sizeType() const;
    virtual void setText(const QString &text);
    QSize sizeHint() const;

    void setMenu(QMenu *menu);

signals:

public slots:

protected:
    void paintEvent(QPaintEvent *);
    void enterEvent(QEvent *e);
    void leaveEvent(QEvent *e);
    void mousePressEvent(QMouseEvent *e);

private:
    RibbonUI::RibbonSizeType mSizeType;
    bool showToolTip;
};

#endif // RIBBONBUTTON_H
