#ifndef RIBBONGALLERYSCROLLBAR_H
#define RIBBONGALLERYSCROLLBAR_H

#include <QScrollArea>
#include <QScrollBar>
#include <QMouseEvent>
#include <QCloseEvent>
#include <QPushButton>
#include "ribbondefs.h"
#include "ribboncontrol.h"

class RIBBONUI_EXPORT RibbonGalleryScrollBar: public QScrollBar {
    Q_OBJECT
public:
    explicit RibbonGalleryScrollBar(QWidget *parent = 0);

protected:
    void paintEvent(QPaintEvent *);
    void enterEvent(QEvent *);
    void leaveEvent(QEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent*e);

private:
    bool hovered;
    bool buttonPressed;
};

#endif // RIBBONGALLERYSCROLLBAR_H
