#include "ribbongallerycornerwidget.h"
#include <QPainter>

RibbonGalleryCornerWidget::RibbonGalleryCornerWidget(QWidget *parent) :
    QPushButton(parent),hovered(false) {
    setFixedSize(15, 17);
    setContentsMargins(0, 0, 0, 0);
}

void RibbonGalleryCornerWidget::enterEvent(QEvent *) {
    hovered = true;
    update(rect());
}

void RibbonGalleryCornerWidget::leaveEvent(QEvent *) {
    hovered = false;
    update(rect());
}

void RibbonGalleryCornerWidget::paintEvent(QPaintEvent *) {
    QPainter p(this);
    p.fillRect(rect(), Qt::white);

    if (hovered) {
        p.setPen(Qt::NoPen);
        p.setBrush(QColor("#cde6f7"));
        p.drawRect(rect());
    }

    if (isDown()) {
        QPen pen2_1;
        pen2_1.setBrush(QColor("#92c0e0"));
        pen2_1.setWidth(1);
        p.setPen(pen2_1);
        p.setBrush(QColor("#92c0e0"));
        p.drawRect(rect());
    }

    p.setPen(QColor(RibbonUI::GALLERY_BORDER_COLOR));
    p.drawLine(0, 0, 1, rect().height());

    p.drawLine(3, 5, 11, 5);

    int top = 7;
    QPointF points_rect[] = {
        QPointF(4, top),
        QPointF(12, top),
        QPointF(7, top+4)
    };
    p.setPen(Qt::NoPen);
    p.setBrush(QColor(RibbonUI::GALLERY_BORDER_COLOR).darker(150));
    p.drawPolygon(points_rect, 3);
}
