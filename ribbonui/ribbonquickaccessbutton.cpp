#include "ribbonquickaccessbutton.h"

RibbonQuickAccessButton::RibbonQuickAccessButton(QWidget *parent) :
    QToolButton(parent),hover(false),mArrowVisible(true)
{
    setStyleSheet("QToolButton {border: 0; outline: 0;} QToolButton::menu-indicator { image: none; }");
    setIconSize(QSize(18, 18));
    setFocusPolicy(Qt::NoFocus);
    setPopupMode(QToolButton::InstantPopup);
    setToolButtonStyle(Qt::ToolButtonIconOnly);

    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

void RibbonQuickAccessButton::setArrowVisible(bool visible) {
    mArrowVisible = visible;
}

QSize RibbonQuickAccessButton::sizeHint() const {
    int width = menu() != 0 ? 24 : 18;
    if (mArrowVisible == false)
        width -= 6;

    return QSize(width, 18);
}

QSize RibbonQuickAccessButton::minimumSize() const {
    return sizeHint();
}

void RibbonQuickAccessButton::paintEvent(QPaintEvent *) {
    QPainter p(this);
    p.fillRect(rect(), Qt::white);

    if (hover) {
        p.fillRect(rect(), QColor("#cde6f7"));
    }

    if (isDown()) {
        p.fillRect(rect(), QColor("#b1d6f0"));
    }

    QPixmap pm = icon().pixmap(18,18);
    p.drawPixmap(0, 0, 18, 18, pm);
}

void RibbonQuickAccessButton::enterEvent(QEvent *) {
    hover = true;
    update(rect());
}

void RibbonQuickAccessButton::leaveEvent(QEvent *) {
    hover = false;
    update(rect());
}
