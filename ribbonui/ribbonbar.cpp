#include "ribbonbar.h"
#include <QHBoxLayout>
#include <QDebug>
#include <QApplication>
#include <QPropertyAnimation>

#include "ribbonwindowbutton.h"
#include "ribbonwindow.h"

RibbonBarToolTipTimer::RibbonBarToolTipTimer(RibbonBarToolTip *tooltip) :
    mToolTip(tooltip){
    setInterval(750);
    setSingleShot(true);
}

RibbonBarToolTip *RibbonBarToolTipTimer::toolTip() {
    return mToolTip;
}

/*******************************************************************************/

RibbonBarToolTip *RibbonBar::toolTipWindow = 0;
RibbonBarToolTipTimer *RibbonBar::mToolTipTimer = 0;

RibbonBar::RibbonBar(QWidget *parent) :
    QMenuBar(parent),mCurrentIndex(-1)
{
    setFixedHeight(151);

    QVBoxLayout *mainLayout = new QVBoxLayout();
    setContentsMargins(0,0,0,0);
    mainLayout->setSpacing(0);
    mainLayout->setContentsMargins(1,1,1,1);
    mTabs = new RibbonTabLayout();
    mStack = new QStackedLayout();
    mStack->setContentsMargins(5, 0, 5, 0);

    RibbonWindow * win = dynamic_cast<RibbonWindow*>(parent);
    if (win != 0) {
        QHBoxLayout *sysButtonRow = new QHBoxLayout();
        sysButtonRow->setSpacing(0);
        mainLayout->addLayout(sysButtonRow);
        sysButtonRow->setContentsMargins(0, 0, 0, 0);
        quickbar = new RibbonQuickbar();
        sysButtonRow->addLayout(quickbar);

        RibbonWindowButton *sysBtnMinimize = new RibbonWindowButton();
        mMaximizeButton = new RibbonWindowButton();
        RibbonWindowButton *sysBtnClose = new RibbonWindowButton();
        sysBtnMinimize->setButtonState(RibbonWindowButton::MINIMIZE);
        mMaximizeButton->setButtonState(RibbonWindowButton::MAXIMIZE);
        sysBtnClose->setButtonState(RibbonWindowButton::CLOSE);
        RibbonWindowHandle *hnd = new RibbonWindowHandle();
        sysButtonRow->addWidget(hnd, 1);
        sysButtonRow->addWidget(sysBtnMinimize);
        sysButtonRow->addWidget(mMaximizeButton);
        sysButtonRow->addWidget(sysBtnClose);

        win->setMoveHandle(hnd);
        // TODO needs to be a signal to the application window!
        connect(sysBtnClose, SIGNAL(clicked()), qApp, SLOT(quit()));
        connect(sysBtnMinimize, SIGNAL(clicked()), win, SLOT(minimize()));
        connect(mMaximizeButton, SIGNAL(clicked()), win, SLOT(maximize()));
    }

    mainLayout->addLayout(mTabs);
    mainLayout->addLayout(mStack);
    setLayout(mainLayout);

    // TODO make it accessible from class method
    RibbonTab *tab = new RibbonTab("File");
    tab->setType(RibbonTab::System);
    mTabs->addWidget(tab);
    mTabList.append(tab);
    connect(tab, SIGNAL(clicked()), this, SLOT(tabClicked()));

    toolTipWindow = new RibbonBarToolTip();
    mToolTipTimer = new RibbonBarToolTipTimer(toolTipWindow);
    connect(mToolTipTimer, SIGNAL(timeout()), this, SLOT(onShowToolTip()));

    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
}

RibbonBar::~RibbonBar() {
    QLayoutItem *item;
    while ((item = mStack->takeAt(0)) != 0) {
        mStack->removeWidget(item->widget());
        delete item->widget();
    }
}

void RibbonBar::setWindowIcon(const QIcon &icon) {
    quickbar->setWindowIcon(icon);
}

void RibbonBar::addPage(RibbonPage *page) {
    RibbonTab *tab = new RibbonTab(page->title());
    mTabs->addWidget(tab);

    QWidget *wid = new QWidget();
    wid->setLayout(page);
    mStack->addWidget(wid);

    if (mCurrentIndex == -1) {
        mCurrentIndex = 0;
        tab->setActive(true);
    }

    mTabList.append(tab);
    connect(tab, SIGNAL(clicked()), this, SLOT(tabClicked()));
}

QSize RibbonBar::minimumSizeHint() const {
    return QSize(mTabs->sizeHint().width(), 151);
}

QSize RibbonBar::sizeHint() const {
    return QSize(mTabs->sizeHint().width(), 151);
}

RibbonQuickbar *RibbonBar::quickAccessBar() {
    return quickbar;
}

void RibbonBar::tabClicked() {
    RibbonTab *current = reinterpret_cast<RibbonTab *>(sender());
    int index = mTabs->indexOf(current);
    if (index == 0)
        return;

    current->setActive(true);
    mCurrentIndex = index;

    RibbonTab *tab;
    foreach (tab, mTabList) {
        if (tab != current) {
            tab->setActive(false);
        }
    }

    mStack->setCurrentIndex(mCurrentIndex-1);
    update();
}

void RibbonBar::paintEvent(QPaintEvent *e) {
    Q_UNUSED(e);
    QPainter p(this);

    // get margins from tabs layout
    int l1, t1, r1, b1;
    mTabs->getContentsMargins(&l1, &t1, &r1, &b1);

    // get margins from main layout
    int l2, t2, r2, b2;
    layout()->getContentsMargins(&l2, &t2, &r2, &b2);

    // reset background
    QRect pane = rect();
    pane.adjust(l2, t2, -r2, -b2);
    //p.fillRect(pane, Qt::white);

    // find maximum tab height
    // find active tab and set X for line painting
    static const int maxTabHeight = 24;
    int tabWidth = 10;
    int x = 0;
    int y = 0;
    RibbonTab *tab;
    foreach (tab, mTabList) {
        if (y < tab->pos().y())
            y = tab->pos().y();

        if (tab->isActive()) {
            x = tab->x();
            tabWidth = tab->width();
        }
    }

    // calculate y position for line
    y += maxTabHeight;

    // draw lines below tabs
    p.setPen(QColor(RibbonUI::BORDER_COLOR));
    p.drawLine((mTabList.at(0)->sizeHint().width()), y, x, y);
    p.drawLine((x+tabWidth)-1, y, rect().width()-r2-1, y);
    p.drawLine(t2, rect().height()-1, rect().width()-r2-1, rect().height()-1);
}

RibbonWindowButton *RibbonBar::maximizeButton() {
    return mMaximizeButton;
}

void RibbonBar::showToolTip(RibbonControl *control) {
    QWidget *content = control->toolTipWidget();
    if (content == 0 || toolTipWindow->isVisible()) {
        return;
    }

    toolTipWindow->setWidget(content);
    toolTipWindow->setGraphicsEffect(0);
    toolTipWindow->move(mapToGlobal(QPoint(control->controlParent()->x()-3, pos().y()+sizeHint().height()-2)));


    mToolTipTimer->start();
}

void RibbonBar::hideToolTip() {
    if (toolTipWindow != 0) {
        toolTipWindow->hide();
    }

    if (mToolTipTimer != 0 && mToolTipTimer->isActive()) {
        mToolTipTimer->stop();
    }
}

void RibbonBar::onShowToolTip() {
    toolTipWindow->show();

    QPropertyAnimation *animate = new QPropertyAnimation(toolTipWindow, "windowOpacity", this);
    animate->setDuration(100);
    animate->setStartValue(0);
    animate->setEndValue(1);
    animate->start();
}
