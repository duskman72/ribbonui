#include "ribbonbutton.h"
#include <QPainter>
#include <QTextDocument>
#include <QStyleOption>
#include <QTextCharFormat>
#include <QTextBlock>
#include <QTextLayout>
#include <QApplication>
#include <QDebug>
#include "ribbonwindow.h"

RibbonButton::RibbonButton(QWidget *parent) :
    QToolButton(parent), RibbonControl(this), mSizeType(RibbonUI::RIBBONTYPE_LARGE),showToolTip(true)
{
    setFixedHeight(70);
    setContentsMargins(0, 0, 0, 0);
    setIconSize(QSize(32, 32));
    setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    setPopupMode(QToolButton::InstantPopup);
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

void RibbonButton::setSizeType(RibbonUI::RibbonSizeType type) {
    mSizeType = type;
    if (mSizeType == RibbonUI::RIBBONTYPE_LARGE) {
        setFixedHeight(70);
        setIconSize(QSize(32, 32));
        setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    } else {
        setFixedHeight(23);
        setIconSize(QSize(16, 16));
        setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    }
    updateGeometry();
}

RibbonUI::RibbonSizeType RibbonButton::sizeType() const {
    return mSizeType;
}

void RibbonButton::setText(const QString &text) {
    QString txt = text;
    txt.replace("\r\n", "\n");
    txt.replace("\n", "<br>");
    QToolButton::setText(text);
    updateGeometry();
}

QSize RibbonButton::sizeHint() const {
    int w = 5;
    int h = 70;

    QFontMetrics fm = fontMetrics();
    if ((mSizeType & RibbonUI::RIBBONTYPE_LARGE) == RibbonUI::RIBBONTYPE_LARGE) {
        QStringList lines = text().split("<br>");

        for (int i = 0; i < lines.length(); i++) {
            int line = fm.width(lines.at(i)) > w ? fm.width(lines.at(i)) + 6 : w;
            w = line;
        }
    } else {
        w += 16; // add width for icon
        w += 3; // add padding after icon

        QString txt = text().replace("<br>", " ");
        txt.replace("\n", " ");
        txt.replace("\r\n", " ");
        w += fm.width(txt);
        w += 5; // add padding after text

        if (menu() != 0) {
            w += 8; // triangle width
            w += 3; // padding after triangle
        }
        h = 26;
    }

    return QSize(w, h);
}

void RibbonButton::setMenu(QMenu *menu) {
    QToolButton::setMenu(menu);
    menu->setStyleSheet(RibbonUI::MENU_STYLESHEET);
}

void RibbonButton::paintEvent(QPaintEvent *) {
    QPainter p(this);
    p.setRenderHint(QPainter::HighQualityAntialiasing);

    QStyleOptionToolButton opt;
    initStyleOption(&opt);

    QColor bg = Qt::transparent;
    if (isEnabled()) {
        if ((opt.state & QStyle::State_MouseOver) == QStyle::State_MouseOver) {
            bg = QColor("#cde6f7");
        }
        if (opt.state & QStyle::State_Sunken) {
            bg = QColor("#b1d6f0");
        }

        if (isDown()) {
            bg = QColor("#b1d6f0");
        }
    }
    p.fillRect(rect(), bg);

    QRect iconRect = rect();
    QRect textRect = rect();
    if ((mSizeType & RibbonUI::RIBBONTYPE_LARGE) == RibbonUI::RIBBONTYPE_LARGE) {
        iconRect.setHeight(iconSize().height());
        iconRect.setWidth(iconSize().width());
    } else {
        iconRect.setWidth(iconSize().width()+5);
        //iconRect.setLeft(5);
    }
    QPixmap pm = icon().pixmap(iconSize());
    p.save();
    if ((mSizeType & RibbonUI::RIBBONTYPE_LARGE) == RibbonUI::RIBBONTYPE_LARGE) {
        p.translate((rect().width() / 2) - (pm.width() / 2), 3);
    } else {
        p.translate(3, (rect().height() / 2) - (pm.height() / 2));
    }
    // check for disabled button state and draw disabled icon
    //QPixmap pm = icon().pixmap(iconSize());
    if (!isEnabled()) {
        QImage img = pm.toImage();
        for (int y = 0; y < img.height(); y++) {
            for (int x = 0; x < img.width(); x++) {
                int pixel = img.pixel(x, y);
                int gray = qGray(pixel);
                int alpha = qAlpha(pixel);
                QColor rgba = QColor(gray, gray, gray, alpha).lighter(140);
                img.setPixel(x, y, qRgba(rgba.red(), rgba.green(), rgba.blue(), rgba.alpha()));
            }
        }
        if ((mSizeType & RibbonUI::RIBBONTYPE_LARGE) == RibbonUI::RIBBONTYPE_LARGE) {
            p.drawPixmap(2, 0, pm);
        } else {
            p.drawPixmap(0, 0, QPixmap::fromImage(img));
        }
    }
    else {
        if ((mSizeType & RibbonUI::RIBBONTYPE_LARGE) == RibbonUI::RIBBONTYPE_LARGE) {
            p.drawPixmap(2, 0, pm);
        } else {
            p.drawPixmap(0, 0, pm);
        }
    }
    p.restore();

    QTextDocument doc;
    QString txt = text();
    if (isEnabled()) {
        if ((mSizeType & RibbonUI::RIBBONTYPE_LARGE) != RibbonUI::RIBBONTYPE_LARGE) {
            txt.replace("<br>", " ");
            txt.replace("\r\n", " ");
            txt.replace("\n", " ");
        }
        doc.setHtml("<style>* {padding: 0;margin:0;color: #444444;}</style><p>"+txt+"</p>");
    } else {
        if ((mSizeType & RibbonUI::RIBBONTYPE_LARGE) != RibbonUI::RIBBONTYPE_LARGE) {
            txt.replace("<br>", " ");
            txt.replace("\r\n", " ");
            txt.replace("\n", " ");
        }
        doc.setHtml("<style>* {padding: 0;margin:0;color: #888888;}</style><p>"+txt+"</p>");
    }
    doc.setDocumentMargin(0);
    if ((mSizeType & RibbonUI::RIBBONTYPE_LARGE) == RibbonUI::RIBBONTYPE_LARGE) {
        doc.setDefaultTextOption(QTextOption(Qt::AlignHCenter));
    } else {
        doc.setDefaultTextOption(QTextOption(Qt::AlignVCenter));
    }
    doc.setTextWidth(textRect.width());

    p.save();
    p.setClipRect(textRect);
    if ((mSizeType & RibbonUI::RIBBONTYPE_LARGE) == RibbonUI::RIBBONTYPE_LARGE) {
       p.translate(0, iconSize().height());
    } else {
       QFontMetrics fm = p.fontMetrics();
       int fh = fm.boundingRect(txt).height();
       p.translate(22, (rect().height() / 2) - (fh / 2));
    }
    doc.drawContents(&p);
    p.restore();

    // draw menu arrow
    if (menu() != 0 || this->arrowType() != Qt::NoArrow) {
        int center;
        int left;
        int right;
        int bottom;
        int top;

        if ((mSizeType & RibbonUI::RIBBONTYPE_LARGE) == RibbonUI::RIBBONTYPE_LARGE) {
            center = sizeHint().width() / 2;
            left = center - 4;
            right = center + 4;
            bottom = rect().height() - 4;
            top = bottom - 3;
        } else {
            center = rect().right() - 10;
            left = center - 4;
            right = center + 4;
            bottom = (rect().height() / 2) + 2;
            top = (rect().height() / 2) - 2;
        }

        QPointF polygon[] = {
            QPointF(left, top),
            QPointF(right, top),
            QPointF(center, bottom)
        };
        p.setPen(Qt::NoPen);
        if (isEnabled())
            p.setBrush(QColor("#666666"));
        else
            p.setBrush(QColor("#999999"));
        p.drawPolygon(polygon, 3);
    }
}

void RibbonButton::enterEvent(QEvent *e) {
    QToolButton::enterEvent(e);
    if (!isEnabled())
        return;

    if (toolTipWidget() != 0 && showToolTip == true) {
        RibbonBar * bar = static_cast<RibbonBar *>(parentWidget()->parentWidget());
        if (bar != 0) {
            bar->showToolTip(this);
        }
    }
}

void RibbonButton::leaveEvent(QEvent *e) {
    QToolButton::leaveEvent(e);
    if (!isEnabled())
        return;

    if (toolTipWidget() != 0) {
        RibbonBar * bar = static_cast<RibbonBar *>(parentWidget()->parentWidget());
        if (bar != 0) {
            bar->hideToolTip();
        }
    }
    showToolTip = true;
}

void RibbonButton::mousePressEvent(QMouseEvent *e) {
    QToolButton::mousePressEvent(e);
    if (!isEnabled())
        return;

    if (toolTipWidget() != 0) {
        RibbonBar * bar = static_cast<RibbonBar *>(parentWidget()->parentWidget());
        if (bar != 0) {
            bar->hideToolTip();
        }
    }
    showToolTip = false;
}

