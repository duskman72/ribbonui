#ifndef RIBBONGROUP_H
#define RIBBONGROUP_H

#include <QWidget>
#include <QLayout>
#include <QLayoutItem>

#include "ribbondefs.h"

class RIBBONUI_EXPORT RibbonGroup : public QLayout
{
    Q_OBJECT
public:
    explicit RibbonGroup(const QString &title, QWidget *parent = 0);
    virtual ~RibbonGroup();
    void addItem(QLayoutItem *item);
    void setGeometry(const QRect &rect);

    virtual QSize sizeHint() const;
    QLayoutItem *itemAt(int index) const;
    QLayoutItem *takeAt(int index);
    int count() const;

signals:

public slots:

private:
    QList<QLayoutItem *> items;
    QString mTitle;
    void doLayout(const QRect &rect);
    bool canlayout;
};

#endif // RIBBONGROUP_H
