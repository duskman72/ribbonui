#include "ribbonpage.h"

static const QString RIBBON_GROUP_SEPARATOR = "ribbongroupseparator";

RibbonPage::RibbonPage(const QString &title, QWidget *parent) :
    QLayout(parent),mTitle(title)
{
    setAlignment(Qt::AlignLeft|Qt::AlignTop);
    setContentsMargins(0, 0, 0, 0);
}

RibbonPage::~RibbonPage() {
    QLayoutItem *item;
    while ((item = takeAt(0)) != 0) {
        removeWidget(item->widget());
        delete item->widget();
    }
}

QString RibbonPage::title() const {
    return mTitle;
}

void RibbonPage::addGroup(RibbonGroup *group) {
    group->setParent(this);
    addItem(group);
}

void RibbonPage::addLayout(RibbonGroup *group) {
    group->setParent(this);
    addItem(group);
}

void RibbonPage::addSeparator() {
    QWidget *sep = new QWidget();
    sep->setFixedWidth(1);
    sep->setFixedHeight(84);
    sep->setContentsMargins(5, 10, 5, 0);
    sep->setObjectName(RIBBON_GROUP_SEPARATOR);
    sep->setStyleSheet("QWidget {background-color: "+RibbonUI::BORDER_COLOR+";}");
    addWidget(sep);
}

void RibbonPage::addItem(QLayoutItem *item) {
    items.append(item);
}

void RibbonPage::setGeometry(const QRect &rect) {
    QLayout::setGeometry(rect);
    doLayout(rect);
}

QSize RibbonPage::sizeHint() const {
    int height = 0;
    int width = 0;
    for (int i = 0; i < items.length(); i++) {
        height = itemAt(i)->sizeHint().height() > height ? itemAt(i)->sizeHint().height(): height;
        width += itemAt(i)->sizeHint().width();
        width += 2;
    }
    return QSize(width, height);
}

QLayoutItem *RibbonPage::itemAt(int index) const {
    return index < items.length() ? items.at(index) : 0;
}

QLayoutItem *RibbonPage::takeAt(int index) {
    return index < items.length() ? items.takeAt(index) : 0;
}

int RibbonPage::count() const {
    return items.length();
}

void RibbonPage::doLayout(const QRect &rect) {
    QLayoutItem *item;
    int x = rect.x();
    static const int y = rect.top();

    foreach (item, items) {
        int l = 0, t = 0, r = 0, b = 0;
        if (item->widget() != 0) {
            item->widget()->getContentsMargins(&l, &t, &r, &b);
            x += l;
        }
        item->setGeometry(QRect(QPoint(x,y), item->sizeHint()));
        x += item->sizeHint().width();
        if (r > 0)
            x += r;
        else
            x += 2;
    }
}
