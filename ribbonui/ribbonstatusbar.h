#ifndef RIBBONSTATUSBAR_H
#define RIBBONSTATUSBAR_H

#include <QStatusBar>
#include "ribbondefs.h"

class RIBBONUI_EXPORT RibbonStatusBar : public QStatusBar
{
    Q_OBJECT
public:
    explicit RibbonStatusBar(QWidget *parent = 0);

signals:

public slots:

};

#endif // RIBBONSTATUSBAR_H
