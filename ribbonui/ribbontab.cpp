#include "ribbontab.h"
#include <QPainter>
#include <QFontMetrics>
#include <QDebug>

RibbonTab::RibbonTab(const QString &label, QWidget *parent) :
    QAbstractButton(parent), mState(None),mType(Default),mPadding(14) {
    setText(label.toUpper());

    QFont f = font();
    f.setPixelSize(13);
    setFont(f);
    setFixedHeight(25);
}

void RibbonTab::setType(RibbonTab::Type type) {
    mType = type;
}

void RibbonTab::paintEvent(QPaintEvent *) {
    QPainter p(this);
    QFontMetrics m = p.fontMetrics();
    int textHeight = m.leading() + m.ascent();
    int textWidth = m.width(text());
    int height = rect().height();
    int textX = (rect().width()/2) - (textWidth/2);
    int textY = ((height/2) + (textHeight/2)) - 2;

    if (isActive()) {
        p.setPen(QColor(RibbonUI::BORDER_COLOR));
        p.drawLine(0, 0, rect().width(), 0);
        p.drawLine(0, 0, 1, rect().height());
        p.drawLine(rect().width(), 0, rect().width()-1, rect().height());
    }

    if ( (mState & MouseOver) || isActive()) {
        if (mType == Default) {
            p.setPen(QColor("#0072c6"));
            p.setBrush(QColor("#0072c6"));
            p.drawText(textX, textY, text());
        } else {
            // draw bg
            p.fillRect(rect(), QColor("#2a8ad4"));
            p.setPen(Qt::white);
            p.setBrush(Qt::white);
            p.drawText(textX, textY, text());
        }
    } else {
        if (mType == Default) {
            p.setPen(QColor("#444444"));
            p.setBrush(QColor("#444444"));
            p.drawText(textX, textY, text());
        } else {
            // draw bg
            p.fillRect(rect(), QColor("#0072c6"));
            p.setPen(Qt::white);
            p.setBrush(Qt::white);
            p.drawText(textX, textY, text());
        }
    }


}

void RibbonTab::enterEvent(QEvent *) {
    mState = static_cast<State>(mState &~ MouseOut);
    mState = static_cast<State>(mState | MouseOver);
    update(rect());
}

void RibbonTab::leaveEvent(QEvent *) {
    mState = static_cast<State>(mState &~ MouseOver);
    mState = static_cast<State>(mState | MouseOut);
    update(rect());
}

QSize RibbonTab::sizeHint() const {
    QFontMetrics m = fontMetrics();
    int width = m.width(text()) + (mPadding*2);
    return QSize(width, 30);
}

QSize RibbonTab::minimumSizeHint() const {
    QFontMetrics m = fontMetrics();
    int width = m.width(text()) + (mPadding*2);
    return QSize(width, 30);
}

void RibbonTab::setActive(bool active) {
    if (!active) {
        mState = static_cast<State>(mState &~ Active);
    } else {
        mState = static_cast<State>(mState | Active);
    }
    update(rect());
}

bool RibbonTab::isActive() const {
    return (mState & Active) == Active;
}
