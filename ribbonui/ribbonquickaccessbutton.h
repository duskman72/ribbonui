#ifndef RIBBONQUICKACCESSBUTTON_H
#define RIBBONQUICKACCESSBUTTON_H

#include <QToolButton>
#include <QPainter>
#include <QWidget>
#include "ribbondefs.h"

class RIBBONUI_EXPORT RibbonQuickAccessButton : public QToolButton
{
    Q_OBJECT
public:
    explicit RibbonQuickAccessButton(QWidget *parent = 0);
    QSize sizeHint() const;
    QSize minimumSize() const;
    void setArrowVisible(bool visible);

signals:

public slots:

protected:
    void paintEvent(QPaintEvent *);
    void enterEvent(QEvent *);
    void leaveEvent(QEvent *);

private:
    bool hover;
    bool mArrowVisible;
};

#endif // RIBBONQUICKACCESSBUTTON_H
