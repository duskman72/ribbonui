#ifndef RIBBONTAB_H
#define RIBBONTAB_H

#include <QAbstractButton>
#include "ribbondefs.h"

class RibbonTab;
typedef QList<RibbonTab*> RibbonTabList;

class RIBBONUI_EXPORT RibbonTab: public QAbstractButton {
    Q_OBJECT
public:
    enum State {
        None = 1,
        MouseOver = 2,
        MouseOut = 4,
        Active = 8
    };

    enum Type {
        Default = 1,
        System = 2
    };

    explicit RibbonTab(const QString &label, QWidget *parent = 0);
    virtual ~RibbonTab(){}
    QSize sizeHint() const;
    QSize minimumSizeHint() const;
    void setType(RibbonTab::Type type);
    void setActive(bool active);
    bool isActive() const;

protected:
    void paintEvent(QPaintEvent *);
    void enterEvent(QEvent *);
    void leaveEvent(QEvent *);

private:
    State mState;
    Type mType;
    int mPadding;
};

#endif // RIBBONTAB_H
