#ifndef RIBBONBAR_H
#define RIBBONBAR_H

#include <QMenuBar>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QStackedLayout>
#include <QPainter>
#include <QTimer>
#include "ribbonwindowbutton.h"
#include "ribbonpage.h"
#include "ribbontab.h"
#include "ribbontablayout.h"
#include "ribbonquickaccessbar.h"
#include "ribboncontrol.h"
#include "ribbonbartooltip.h"
#include "ribbondefs.h"

class RIBBONUI_EXPORT RibbonBarToolTipTimer : public QTimer {
    Q_OBJECT
public:
    explicit RibbonBarToolTipTimer(RibbonBarToolTip *tooltip);
    RibbonBarToolTip *toolTip();

private:
    RibbonBarToolTip *mToolTip;
};

class RIBBONUI_EXPORT RibbonBar : public QMenuBar {
    Q_OBJECT
public:
    explicit RibbonBar(QWidget *parent = 0);
    virtual ~RibbonBar();
    void addPage(RibbonPage *page);
    QSize sizeHint() const;
    QSize minimumSizeHint() const;
    RibbonWindowButton *maximizeButton();
    void setWindowIcon(const QIcon &icon);
    RibbonQuickbar *quickAccessBar();
    void showToolTip(RibbonControl *control);
    void hideToolTip();

protected:
    void paintEvent(QPaintEvent *e);

signals:

public slots:
    void tabClicked();
    void onShowToolTip();

private:
    int mCurrentIndex;
    RibbonQuickbar *quickbar;
    RibbonTabLayout *mTabs;
    QStackedLayout *mStack;
    RibbonTabList mTabList;
    RibbonWindowButton *mMaximizeButton;
    static RibbonBarToolTipTimer *mToolTipTimer;
    static RibbonBarToolTip *toolTipWindow;
};

#endif // RIBBONBAR_H
