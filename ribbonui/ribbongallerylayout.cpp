#include "ribbongallerylayout.h"
#include <QDebug>

RibbonGalleryLayout::RibbonGalleryLayout(QWidget *parent) :
    QLayout(parent) {
    //setSizeConstraint(QLayout::SetDefaultConstraint);
    setContentsMargins(0, 0, 0, 0);
}

RibbonGalleryLayout::~RibbonGalleryLayout() {
    QLayoutItem *item;
    while ((item = takeAt(0)) != 0) {
        removeWidget(item->widget());
        delete item->widget();
    }
}

void RibbonGalleryLayout::addItem(QLayoutItem *item) {
    items.append(item);
}

void RibbonGalleryLayout::addWidget(RibbonGalleryItem *item) {
    QLayout::addWidget(item);
}

void RibbonGalleryLayout::setGeometry(const QRect &rect) {
    QLayout::setGeometry(rect);
    doLayout();
}

QSize RibbonGalleryLayout::sizeHint() const {
    int leftWidth = 1;
    int rightWidth = 1;
    int height = 1;

    // find widest items
    for (int i = 0; i < count(); i++) {
        QLayoutItem *item = itemAt(i);
        if(i%2 == 0) {
            // left
            if (item->sizeHint().width() > leftWidth)
                leftWidth = item->sizeHint().width();
        } else {
            // right
            if (item->sizeHint().width() > rightWidth)
                rightWidth = item->sizeHint().width();
        }
    }

    for (int i = 0; i < count(); i++) {
        QLayoutItem *item = itemAt(i);
        if (i%2 == 0) {
            height += item->sizeHint().height();
        }
    }

    return QSize(leftWidth+rightWidth, height);
}

QSize RibbonGalleryLayout::minimumSize() const {
    return sizeHint();
}

QLayoutItem *RibbonGalleryLayout::itemAt(int index) const {
    return items.length() > index ? items.at(index) : 0;
}

QLayoutItem *RibbonGalleryLayout::takeAt(int index) {
    return items.length() > index ? items.takeAt(index) : 0;
}

int RibbonGalleryLayout::count() const {
    return items.size();
}

void RibbonGalleryLayout::doLayout() {
    int leftWidth = 0;
    int rightWidth = 0;

    // find widest items
    for (int i = 0; i < count(); i++) {
        QLayoutItem *item = itemAt(i);
        if(i%2 == 0) {
            // left
            if (item->sizeHint().width() > leftWidth)
                leftWidth = item->sizeHint().width();
        } else {
            // right
            if (item->sizeHint().width() > rightWidth)
                rightWidth = item->sizeHint().width();
        }
    }

    int y = 0;
    for (int i = 0; i < count(); i++) {
        QLayoutItem *item = itemAt(i);
        if (i%2 == 0 && i > 0)
            y += item->sizeHint().height();

        if(i%2 == 0) {
            // left
            item->setGeometry(QRect(QPoint(0, y), QSize(leftWidth, item->sizeHint().height())));
        } else {
            // right
            item->setGeometry(QRect(QPoint(leftWidth, y), QSize(rightWidth, item->sizeHint().height())));
        }
    }
}
