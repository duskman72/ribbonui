#-------------------------------------------------
#
# Project created by QtCreator 2014-05-22T10:24:28
#
#-------------------------------------------------

QT       += core gui winextras

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ../ribbonui
TEMPLATE = lib
DEFINES += RIBBONUI_LIB

INCLUDEPATH += . ..

SOURCES += ribbonbar.cpp \
    ribbonpage.cpp \
    ribbongroup.cpp \
    ribbonbutton.cpp \
    ribbontab.cpp \
    ribbongallery.cpp \
    ribbonwindow.cpp \
    ribbonwindowbutton.cpp \
    ribbonstatusbar.cpp \
    ribbontablayout.cpp \
    ribbonquickaccessbutton.cpp \
    ribbonquickaccessbar.cpp \
    ribboncontrol.cpp \
    ribbonbartooltip.cpp \
    ribbongalleryscrollbar.cpp \
    ribbongallerycornerwidget.cpp \
    ribbongallerypopup.cpp \
    ribbongalleryitem.cpp \
    ribbongallerylayout.cpp

HEADERS  += ribbonbar.h \
    ribbonpage.h \
    ribbongroup.h \
    ribbonbutton.h \
    ribbontab.h \
    ribbondefs.h \
    ribbongallery.h \
    ribbonwindow.h \
    ribbonwindowbutton.h \
    ribbonstatusbar.h \
    ribbontablayout.h \
    ribbonquickaccessbutton.h \
    ribbonquickaccessbar.h \
    ribboncontrol.h \
    ribbonbartooltip.h \
    ribbongalleryscrollbar.h \
    ribbongallerycornerwidget.h \
    ribbongallerypopup.h \
    ribbongalleryitem.h \
    ribbongallerylayout.h

LIBS += -lDwmapi
