#ifndef RIBBONPAGE_H
#define RIBBONPAGE_H

#include <QWidget>
#include <QLayout>
#include "ribbongroup.h"
#include "ribbondefs.h"

class RIBBONUI_EXPORT RibbonPage : public QLayout
{
    Q_OBJECT
public:
    explicit RibbonPage(const QString &title, QWidget *parent = 0);
    virtual ~RibbonPage();
    QString title() const;
    void addGroup(RibbonGroup *group);
    void addLayout(RibbonGroup *group);
    void addSeparator();

    void addItem(QLayoutItem *item);
    void setGeometry(const QRect &rect);

    QSize sizeHint() const;
    QLayoutItem *itemAt(int index) const;
    QLayoutItem *takeAt(int index);
    int count() const;

signals:

public slots:

private:
    QString mTitle;
    QList<QLayoutItem *> items;
    void doLayout(const QRect &rect);
};

#endif // RIBBONPAGE_H
