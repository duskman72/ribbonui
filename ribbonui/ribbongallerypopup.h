#ifndef RIBBONGALLERYPOPUP_H
#define RIBBONGALLERYPOPUP_H

#include <QMouseEvent>
#include <QCloseEvent>
#include <QPaintEvent>
#include <QWidget>
#include "ribbondefs.h"

class RIBBONUI_EXPORT RibbonGalleryPopup: public QWidget {
    Q_OBJECT
public:
    RibbonGalleryPopup(QWidget *parent = 0);

protected:
    void paintEvent(QPaintEvent *);
    void closeEvent(QCloseEvent *e);

signals:
    void closed();
};

#endif // RIBBONGALLERYPOPUP_H
