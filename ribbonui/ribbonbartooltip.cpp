#include "ribbonbartooltip.h"
#include <QHBoxLayout>
#include <QPainter>
#include <QGuiApplication>
#include <QScreen>
#include <QApplication>
#include <QDesktopWidget>
#include <QDebug>

RibbonBarToolTip::RibbonBarToolTip(QWidget *parent) :
    QWidget(parent, Qt::WindowFlags(Qt::Tool|Qt::FramelessWindowHint))
{
    setFocusPolicy(Qt::NoFocus);
    setAttribute(Qt::WA_CustomWhatsThis);
    setAttribute(Qt::WA_DeleteOnClose, true);
    setObjectName("TOOLTIP_POPUP");

    QHBoxLayout *l = new QHBoxLayout();
    l->setContentsMargins(18,18,18,18);
    setLayout(l);
}

void RibbonBarToolTip::setWidget(QWidget *wid) {
    while (layout()->takeAt(0) != 0);
    wid->setContentsMargins(0, 0, 0, 0);
    layout()->addWidget(wid);
}

void RibbonBarToolTip::showEvent(QShowEvent *) {
    background = QGuiApplication::primaryScreen()->grabWindow(QApplication::desktop()->internalWinId(), x(), y(), width(), height());
}

void RibbonBarToolTip::paintEvent(QPaintEvent *e) {
    QWidget::paintEvent(e);

    QPainter p(this);
    p.setRenderHint(QPainter::HighQualityAntialiasing);
    p.drawPixmap(0, 0, background);

    // background & border
    QRect r = rect();
    r.setLeft(4);
    r.setTop(4);
    r.setRight(r.right()-4);
    r.setBottom(r.bottom()-4);
    QPen pen;
    pen.setWidth(1);
    pen.setBrush(QColor(RibbonUI::BORDER_COLOR).darker(120));
    p.setPen(pen);
    p.setBrush(Qt::white);
    p.drawRect(r);

    // drop shadow
    p.setBrush(Qt::NoBrush);
    QRect r1(3,3,rect().width()-6, rect().height()-6);
    p.setPen(QColor(0, 0, 0, 40));
    p.drawRect(r1);

    r1 = QRect(2,2,rect().width()-4, rect().height()-4);
    p.setPen(QColor(0, 0, 0, 20));
    p.drawRect(r1);

    r1 = QRect(1,1,rect().width()-2, rect().height()-2);
    p.setPen(QColor(0, 0, 0, 10));
    p.drawRect(r1);
}
