#ifndef RIBBONGALLERYCORNERWIDGET_H
#define RIBBONGALLERYCORNERWIDGET_H

#include <QPushButton>
#include "ribbondefs.h"

class RIBBONUI_EXPORT RibbonGalleryCornerWidget: public QPushButton {
    Q_OBJECT
public:
    explicit RibbonGalleryCornerWidget(QWidget *parent = 0);

protected:
    void paintEvent(QPaintEvent *);
    void enterEvent(QEvent *);
    void leaveEvent(QEvent *);

private:
    bool hovered;
};

#endif // RIBBONGALLERYCORNERWIDGET_H
