#ifndef RIBBONQUICKBAR_H
#define RIBBONQUICKBAR_H

#include <QLayout>
#include <QLabel>
#include <QIcon>
#include "ribbondefs.h"
#include "ribbonquickaccessbutton.h"

class RIBBONUI_EXPORT RibbonQuickbar : public QLayout
{
    Q_OBJECT
public:
    explicit RibbonQuickbar(QWidget *parent = 0);
    QSize sizeHint() const;
    QSize minimumSize() const;
    QSize maximumSize() const;
    void addItem(QLayoutItem *item);
    void addWidget(RibbonQuickAccessButton *w);
    QLayoutItem *itemAt(int index) const;
    QLayoutItem *takeAt(int index);
    int count() const;
    void setWindowIcon(const QIcon &icon);
    void setGeometry(const QRect &rect);

signals:

public slots:

private:
    QList<QLayoutItem *> layoutitems;
    RibbonQuickAccessButton *windowIcon;
    RibbonQuickAccessButton *quickaccessMenuIcon;
    void doLayout(const QRect &rect);
};

#endif // RIBBONQUICKBAR_H
