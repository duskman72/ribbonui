#include "ribbonwindow.h"
#include <QDebug>

RibbonWindowHandle::RibbonWindowHandle(QWidget *parent) :
    QLabel(parent),down(false) {
    setMouseTracking(true);
    setAlignment(Qt::AlignCenter);
    setFixedHeight(22);
    setStyleSheet("QLabel {color: #444444;}");

    QFont f = font();
    f.setPixelSize(13);
    setFont(f);
}

void RibbonWindowHandle::mousePressEvent(QMouseEvent *e) {
    if (e->button() == Qt::LeftButton) {
        down = true;
        emit moveRequest(e);
    }
}

void RibbonWindowHandle::mouseDoubleClickEvent(QMouseEvent *e) {
    if (e->button() == Qt::LeftButton) {
        down = true;
        emit doubleClickEvent(e);
    }
}

void RibbonWindowHandle::mouseReleaseEvent(QMouseEvent *e) {
    QLabel::mouseReleaseEvent(e);
    down = false;
}

void RibbonWindowHandle::mouseMoveEvent(QMouseEvent *e) {
    if (down) {
        QLabel::mouseMoveEvent(e);
        emit moveEvent(e);
    }
}

/*****************************************************************************************************/

RibbonWindow::RibbonWindow(QWidget *parent) :
    QMainWindow(parent),mRibbonBar(0)
{
    setWindowFlags(Qt::WindowFlags(windowFlags()|Qt::FramelessWindowHint));
    setStyleSheet("QMainWindow {background-color: #ffffff; border: 1px solid #0072c6;}");
    setCentralWidget(new QWidget());

    mRibbonBar = new RibbonBar(this);

    QPixmap pm(32, 32);
    QPainter p(&pm);
    p.fillRect(pm.rect(), QColor("#0072c6"));
    QPen pen;
    pen.setBrush(Qt::white);
    p.setBrush(Qt::white);
    p.setPen(pen);
    QFont f = p.font();
    f.setPixelSize(24);
    p.setFont(f);
    p.drawText(10, 25, "?");
    QMainWindow::setWindowIcon(QIcon(pm));

    RibbonStatusBar *sb = new RibbonStatusBar();
    setStatusBar(sb);
}

RibbonWindow::~RibbonWindow() {
    delete mRibbonBar;
}

RibbonBar *RibbonWindow::ribbonBar() {
    setMenuBar(mRibbonBar);
    return mRibbonBar;
}

void RibbonWindow::setWindowIcon(const QIcon &icon) {
    ribbonBar()->setWindowIcon(icon);
    QMainWindow::setWindowIcon(icon);
}

void RibbonWindow::changeEvent(QEvent *e) {
    QMainWindow::changeEvent(e);
    if(e->type() == QEvent::WindowStateChange) {
        if (isMinimized()) {
            RibbonBar *bar = mRibbonBar;
            if (bar != 0) {
                RibbonWindowButton *btn = bar->maximizeButton();
                if (btn != 0)
                    btn->setButtonState(RibbonWindowButton::MAXIMIZE);
            }
            return;
        }
        if (isMaximized()) {
            RibbonBar *bar = mRibbonBar;
            if (bar != 0) {
                RibbonWindowButton *btn = bar->maximizeButton();
                if (btn != 0)
                    btn->setButtonState(RibbonWindowButton::MAXIMIZED);
            }
            setStyleSheet("QMainWindow {background-color: white; border: 0;}");
            return;
        }
        if (this->isActiveWindow()) {
            RibbonBar *bar = mRibbonBar;
            if (bar != 0) {
                RibbonWindowButton *btn = bar->maximizeButton();
                if (btn != 0)
                    btn->setButtonState(RibbonWindowButton::MAXIMIZE);
            }
            return;
        }
    }
}

void RibbonWindow::minimize() {
    showMinimized();
}

void RibbonWindow::maximize() {
    if (isMaximized()) {
        showNormal();
    }
    else {
        showMaximized();
    }
}

void RibbonWindow::setWindowTitle(const QString &title){
    mMoveHandle->setText(title);
}

void RibbonWindow::setMoveHandle(RibbonWindowHandle *handle) {
    mMoveHandle = handle;
    connect(handle, SIGNAL(moveRequest(QMouseEvent *)), this, SLOT(onWindowMoveRequest(QMouseEvent *)));
    connect(handle, SIGNAL(moveEvent(QMouseEvent *)), this, SLOT(onWindowMove(QMouseEvent *)));
    connect(handle, SIGNAL(doubleClickEvent(QMouseEvent*)), this, SLOT(onResizeWindow(QMouseEvent *)));
}

void RibbonWindow::onWindowMoveRequest(QMouseEvent *e) {
    if (isMaximized())
        return;

    oldPos = e->globalPos();
}

void RibbonWindow::onResizeWindow(QMouseEvent *e) {
    Q_UNUSED(e);

    if (isMaximized())
        showNormal();
    else
        showMaximized();
}

void RibbonWindow::onWindowMove(QMouseEvent *e) {
    if (isMaximized())
        return;

    const QPoint delta = e->globalPos() - oldPos;
    bool locked = false;
    if (locked)
        // if locked, ignore delta on y axis, stay at the top
        move(x()+delta.x(), y());
    else
        move(x()+delta.x(), y()+delta.y());
    oldPos = e->globalPos();
}

void RibbonWindow::show() {
    activateWindow();
    QMainWindow::show();
    RibbonBar *bar = mRibbonBar;
    if (bar != 0) {
        RibbonWindowButton *btn = bar->maximizeButton();
        if (btn != 0)
            btn->setButtonState(RibbonWindowButton::MAXIMIZE);
        setStyleSheet("QMainWindow {background-color: white; border: 1px solid #0072c6;}");
    }
}

void RibbonWindow::showNormal() {
    activateWindow();
    QMainWindow::showNormal();
    RibbonBar *bar = mRibbonBar;
    if (bar != 0) {
        RibbonWindowButton *btn = bar->maximizeButton();
        if (btn != 0)
            btn->setButtonState(RibbonWindowButton::MAXIMIZE);
        menuBar()->layout()->setContentsMargins(1, 1, 1, 1);
        setStyleSheet("QMainWindow {background-color: white; border: 1px solid #0072c6;}");
    }
}

void RibbonWindow::showMinimized() {
    activateWindow();
    QMainWindow::showMinimized();
    RibbonBar *bar = mRibbonBar;
    if (bar != 0) {
        RibbonWindowButton *btn = bar->maximizeButton();
        if (btn != 0)
            btn->setButtonState(RibbonWindowButton::MAXIMIZE);
        setStyleSheet("QMainWindow {background-color: white; border: 1px solid #0072c6;}");
    }
}

void RibbonWindow::showMaximized() {
    activateWindow();
    QMainWindow::showMaximized();

    RibbonBar *bar = mRibbonBar;
    if (bar != 0) {
        RibbonWindowButton *btn = bar->maximizeButton();
        if (btn != 0)
            btn->setButtonState(RibbonWindowButton::MAXIMIZED);
        menuBar()->layout()->setContentsMargins(0, 0, 0, 4);
        setStyleSheet("QMainWindow {background-color: white; border: 0;}");
    }
}

#ifdef Q_OS_WIN32
bool RibbonWindow::event(QEvent *event) {
    return QMainWindow::event(event);
}

#endif
