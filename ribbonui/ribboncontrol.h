#ifndef RIBBONCONTROL_H
#define RIBBONCONTROL_H

#include <QWidget>
#include "ribbondefs.h"

class RIBBONUI_EXPORT RibbonControl
{
public:
    explicit RibbonControl(QWidget *parent);
    QWidget *controlParent();
    void setToolTipWidget(QWidget *wid);
    QWidget *toolTipWidget();
    virtual void setSizeType(RibbonUI::RibbonSizeType type) = 0;
    virtual RibbonUI::RibbonSizeType sizeType() const = 0;

private:
    QWidget *mParent;
    QWidget *mTooltip;
};

#endif // RIBBONCONTROL_H
