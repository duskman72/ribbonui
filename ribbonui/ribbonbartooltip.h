#ifndef RIBBONBARTOOLTIP_H
#define RIBBONBARTOOLTIP_H

#include <QWidget>
#include <QPaintEvent>
#include <QShowEvent>
#include <QPixmap>
#include "ribbondefs.h"

class RIBBONUI_EXPORT RibbonBarToolTip : public QWidget
{
    Q_OBJECT
public:
    explicit RibbonBarToolTip(QWidget *parent = 0);
    void setWidget(QWidget *wid);

protected:
    void showEvent(QShowEvent *);
    void paintEvent(QPaintEvent *e);

signals:

public slots:

private:
    QPixmap background;
};

#endif // RIBBONBARTOOLTIP_H
