#include "ribbongalleryscrollbar.h"
#include <QPainter>
#include <QCursor>

RibbonGalleryScrollBar::RibbonGalleryScrollBar(QWidget *parent):
    QScrollBar(parent),hovered(false),buttonPressed(false) {
    setMouseTracking(true);

    QString css2;
    css2 += "QScrollBar:vertical {margin: 32px 0px 33px 0px;width: 15px;}";
    css2 += "QScrollBar::handle:vertical {outline: 0;min-height: 0px; height: 0px;width: 0px;margin: 0px; }";
    css2 += "QScrollBar::sub-line:vertical {subcontrol-position: top;subcontrol-origin: margin;height: 35px;}";
    css2 += "QScrollBar::add-line:vertical {subcontrol-position: bottom;subcontrol-origin: margin;height: 28px;}";
    setStyleSheet(css2);
    setContentsMargins(0, 0, 0, 0);
}

void RibbonGalleryScrollBar::enterEvent(QEvent *) {
    hovered = true;
    update(rect());
}

void RibbonGalleryScrollBar::leaveEvent(QEvent *) {
    hovered = false;
    update(rect());
}

void RibbonGalleryScrollBar::mouseMoveEvent(QMouseEvent *) {
    update(rect());
}

void RibbonGalleryScrollBar::mousePressEvent(QMouseEvent *e) {
    if (e->button() == Qt::LeftButton) {
        buttonPressed = true;
        QScrollBar::mousePressEvent(e);
    }
    update(rect());
}

void RibbonGalleryScrollBar::mouseReleaseEvent(QMouseEvent *e) {
    buttonPressed = false;
    QScrollBar::mouseReleaseEvent(e);
    update(rect());
}

void RibbonGalleryScrollBar::paintEvent(QPaintEvent *) {
    QPainter p(this);

    p.fillRect(rect(), Qt::white);
    if (isEnabled())
        p.setPen(QColor(RibbonUI::GALLERY_BORDER_COLOR));
    else
        p.setPen(QColor(RibbonUI::GALLERY_BORDER_COLOR_DISABLED));
    // left line
    p.drawLine(0, 0, 1, rect().height());
    // bottom line
    p.drawLine(0, rect().height()-1, rect().width(), rect().height()-1);
    // separator line
    p.drawLine(0, rect().height()/2, rect().width(), rect().height()/2);

    QPoint pos = mapFromGlobal(QCursor::pos());
    QRect topRect = QRect(0, -1, rect().width(), (rect().height()/2)+1);
    QRect bottomRect = QRect(0, (rect().height()/2), rect().width(), topRect.height()-1);

    if (hovered && isEnabled()) {
        if (topRect.contains(pos)) {
            QPen pen1;
            pen1.setBrush(QColor("#92c0e0"));
            pen1.setWidth(1);
            p.setPen(pen1);
            p.setBrush(QColor("#cde6f7"));
            p.drawRect(topRect);
        }
        if (bottomRect.contains(pos)) {
            QPen pen1;
            pen1.setBrush(QColor("#92c0e0"));
            pen1.setWidth(1);
            p.setPen(pen1);
            p.setBrush(QColor("#cde6f7"));
            p.drawRect(bottomRect);
        }
    }

    if (buttonPressed && isEnabled()) {
        if (topRect.contains(pos)) {
            QPen pen1_1;
            pen1_1.setBrush(QColor("#92c0e0"));
            pen1_1.setWidth(1);
            p.setPen(pen1_1);
            p.setBrush(QColor("#92c0e0"));
            p.drawRect(topRect);
        }
        if (bottomRect.contains(pos)) {
            QPen pen1_1;
            pen1_1.setBrush(QColor("#92c0e0"));
            pen1_1.setWidth(1);
            p.setPen(pen1_1);
            p.setBrush(QColor("#92c0e0"));
            p.drawRect(bottomRect);
        }
    }

    int top = topRect.center().y();
    QPointF points_top[] = {
        QPointF(7, top),
        QPointF(12, top+4),
        QPointF(4, top+4)
    };
    top = bottomRect.center().y();
    QPointF points_bottom[] = {
        QPointF(4, top),
        QPointF(12, top),
        QPointF(7, top+4)
    };
    p.setPen(Qt::NoPen);
    p.setBrush(QColor(RibbonUI::GALLERY_BORDER_COLOR).darker(150));
    p.drawPolygon(points_top, 3);
    p.drawPolygon(points_bottom, 3);
}
