#ifndef RIBBONWINDOWBUTTON_H
#define RIBBONWINDOWBUTTON_H

#include <QPushButton>
#include <QPainter>
#include "ribbondefs.h"

class RIBBONUI_EXPORT RibbonWindowButton : public QPushButton
{
    Q_OBJECT
public:
    enum ButtonState {
        CLOSE = 1,
        MINIMIZE = 2,
        MAXIMIZE = 4,
        MAXIMIZED = 8
    };

    explicit RibbonWindowButton(QWidget *parent = 0);
    void setButtonState(RibbonWindowButton::ButtonState state);
    QSize minimumSize() const;
    QSize maximumSize() const;

signals:

public slots:


protected:
    void enterEvent(QEvent *);
    void leaveEvent(QEvent *);
    void paintEvent(QPaintEvent *);

private:
    bool hovered;
    ButtonState mButtonState;
};

#endif // RIBBONWINDOWBUTTON_H
