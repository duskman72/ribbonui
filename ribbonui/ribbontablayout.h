#ifndef RIBBONTABLAYOUT_H
#define RIBBONTABLAYOUT_H

#include <QLayout>
#include <QLayoutItem>
#include "ribbondefs.h"

class RIBBONUI_EXPORT RibbonTabLayout : public QLayout
{
    Q_OBJECT
public:
    explicit RibbonTabLayout(QWidget *parent = 0);
    virtual ~RibbonTabLayout();
    void addItem(QLayoutItem *item);
    void setGeometry(const QRect &rect);

    QSize sizeHint() const;
    QLayoutItem *itemAt(int index) const;
    QLayoutItem *takeAt(int index);
    int count() const;

signals:

public slots:

private:
    QList<QLayoutItem *> items;
    void doLayout(const QRect &rect);
};

#endif // RIBBONTABLAYOUT_H
