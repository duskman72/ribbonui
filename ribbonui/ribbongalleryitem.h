#ifndef RIBBONGALLERYITEM_H
#define RIBBONGALLERYITEM_H

#include <QToolButton>
#include "ribbonbutton.h"
#include "ribbondefs.h"

class RIBBONUI_EXPORT RibbonGalleryItem : public RibbonButton
{
    Q_OBJECT
public:
    explicit RibbonGalleryItem(QWidget *parent = 0);

signals:

public slots:

};

#endif // RIBBONGALLERYITEM_H
