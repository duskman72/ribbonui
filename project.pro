#-------------------------------------------------
#
# Project created by QtCreator 2014-04-01T15:12:40
#
#-------------------------------------------------

QT       += core gui network webkitwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = project
TEMPLATE = subdirs

DEFINES += RIBBONUI_LIB

SUBDIRS += ribbonui \
    demo

LIBS += -L../ribbonui -lribbonui
