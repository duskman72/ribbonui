#include "mainwindow.h"
#include "ribbonbar.h"
#include "ribbongroup.h"
#include "ribbonpage.h"
#include "ribbongallery.h"
#include "ribbonbutton.h"
#include "ribbonquickaccessbar.h"
#include "ribbonquickaccessbutton.h"
#include "ribbongalleryitem.h"

#include <QStatusBar>
#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QToolButton>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : RibbonWindow(parent)
{
    RibbonBar *bar = ribbonBar();
    RibbonPage *page1 = new RibbonPage("Large Layout");
    RibbonPage *page2 = new RibbonPage("Medium Layout");
    bar->addPage(page1);
    bar->addPage(page2);

    RibbonWindow::setWindowTitle("RibbonUI Demo");

    // PAGE #1 setup
    RibbonGroup *grp = new RibbonGroup("Group 1");
    RibbonGroup *p1_grp2 = new RibbonGroup("Group 2");
    page1->addGroup(grp);
    page1->addSeparator();
    page1->addGroup(p1_grp2);

    RibbonGallery *gal = new RibbonGallery();
    grp->addWidget(gal);

    RibbonButton *btn1 = new RibbonButton();
    btn1->setText("Simple Button");
    btn1->setIcon(QIcon(":/res/file28.png"));
    p1_grp2->addWidget(btn1);

    RibbonButton *btn2 = new RibbonButton();
    btn2->setText("Button<br>disabled");
    btn2->setIcon(QIcon(":/res/file28.png"));
    btn2->setDisabled(true);
    p1_grp2->addWidget(btn2);

    RibbonButton *btn3 = new RibbonButton();
    btn3->setText("Button with<br>tooltip");
    btn3->setIcon(QIcon(":/res/file28.png"));
    p1_grp2->addWidget(btn3);
    QLabel *tooltipLabel = new QLabel("<style>* {font-size: 13px; color: #444444;}</style><p><strong>Tooltip Info</strong></p><p>This is a tooltip widget, it is made<br>to show simple text or a widget</p>");
    btn3->setToolTipWidget(tooltipLabel);

    RibbonButton *btn4 = new RibbonButton();
    btn4->setText("Button with<br>menu");
    btn4->setIcon(QIcon(":/res/file28.png"));
    p1_grp2->addWidget(btn4);

    QMenu *menu_btn4 = new QMenu();
    menu_btn4->addAction(QIcon(":/res/file16.png"), "Item #1");
    menu_btn4->addAction(QIcon(":/res/file16.png"), "Item #3");
    menu_btn4->addAction(QIcon(":/res/file16.png"), "Item #3");
    menu_btn4->addSeparator();
    menu_btn4->addAction(QIcon(":/res/file16.png"), "Item #4");
    btn4->setMenu(menu_btn4);

    statusBar()->addWidget(new QLabel("ready"));

    for (int i = 0; i < 8; i++) {
        QAction *a = new QAction(QIcon(":/res/file16.png"), QString("Gallery Item #%1").arg(i), 0);
        gal->addAction(a);
    }

    gal->setIcon(QIcon(":/res/gallery.png"));
    gal->setText("Medium Gallery<br>Widget");

    // PAGE #2 setup

    RibbonGroup *page2_grp1 = new RibbonGroup("Group 1");
    RibbonGroup *page2_grp2 = new RibbonGroup("Group 2");
    page2->addGroup(page2_grp1);
    page2->addSeparator();
    page2->addGroup(page2_grp2);

    RibbonGallery *gal2 = new RibbonGallery();
    gal2->setSizeType(RibbonUI::RIBBONTYPE_MEDIUM);
    gal2->setIcon(QIcon(":/res/file28.png"));
    gal2->setText("Medium Gallery<br>Widget");
    for (int i = 0; i < 8; i++) {
        QAction *a = new QAction(QIcon(":/res/file16.png"), QString("Gallery Item #%1").arg(i), 0);
        gal2->addAction(a);
    }
    page2_grp1->addWidget(gal2);

    RibbonButton *page2_btn1 = new RibbonButton();
    page2_btn1->setText("Simple button<br>with menu");
    page2_btn1->setIcon(QIcon(":/res/file28.png"));
    page2_grp2->addWidget(page2_btn1);

    QMenu *page2_menu_btn1 = new QMenu();
    page2_menu_btn1->addAction(QIcon(":/res/file16.png"), "Item #1");
    page2_menu_btn1->addAction(QIcon(":/res/file16.png"), "Item #3");
    page2_menu_btn1->addAction(QIcon(":/res/file16.png"), "Item #3");
    page2_menu_btn1->addSeparator();
    page2_menu_btn1->addAction(QIcon(":/res/file16.png"), "Item #4");
    page2_btn1->setMenu(page2_menu_btn1);

    RibbonButton *page2_btn3 = new RibbonButton();
    page2_btn3->setSizeType(RibbonUI::RIBBONTYPE_SMALL);
    page2_btn3->setDisabled(true);
    page2_btn3->setText("Disabled Button");
    page2_btn3->setIcon(QIcon(":/res/file16.png"));
    page2_grp2->addWidget(page2_btn3);

    RibbonButton *page2_btn2 = new RibbonButton();
    page2_btn2->setSizeType(RibbonUI::RIBBONTYPE_SMALL);
    page2_btn2->setText("Button width menu");
    page2_btn2->setIcon(QIcon(":/res/file16.png"));
    page2_grp2->addWidget(page2_btn2);

    QMenu *menu_btn2 = new QMenu();
    menu_btn2->addAction(QIcon(":/res/file16.png"), "Item #1");
    menu_btn2->addAction(QIcon(":/res/file16.png"), "Item #3");
    menu_btn2->addAction(QIcon(":/res/file16.png"), "Item #3");
    menu_btn2->addSeparator();
    menu_btn2->addAction(QIcon(":/res/file16.png"), "Item #4");
    page2_btn2->setMenu(menu_btn2);

    RibbonButton *page2_btn4 = new RibbonButton();
    page2_btn4->setSizeType(RibbonUI::RIBBONTYPE_SMALL);
    page2_btn4->setText("Simple Button");
    page2_btn4->setIcon(QIcon(":/res/file16.png"));
    page2_grp2->addWidget(page2_btn4);
}

MainWindow::~MainWindow()
{

}
