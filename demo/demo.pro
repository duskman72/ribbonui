#-------------------------------------------------
#
# Project created by QtCreator 2014-05-28T18:59:40
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = demo
TEMPLATE = app
INCLUDEPATH += ../ribbonui

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

LIBS += -L../ribbonui -lribbonui

RESOURCES += \
    resources.qrc
