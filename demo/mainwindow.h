#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ribbonwindow.h"

class MainWindow : public RibbonWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
};

#endif // MAINWINDOW_H
